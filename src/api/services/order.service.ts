import { orderModel } from '../models/order.model';
import { baseService } from './base.service';
import {IOrder, IOrderDocument} from '../interfaces/order.interface';

export class orderService extends baseService<IOrderDocument, typeof orderModel> {

    constructor() {
        super(orderModel);
    }

}
