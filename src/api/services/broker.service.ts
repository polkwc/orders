import client, { Connection, Channel, ConsumeMessage } from 'amqplib';
import config  from '../config';
import { IMessage } from '../interfaces/message.interface';
import { IOrderDocument } from '../interfaces/order.interface';
import { authValid } from '../services/auth.service';
import { orderService } from '../services/order.service';

const consumer = (channel: Channel) => async (msg: ConsumeMessage | null): Promise<void> => {
    if (msg) {
        const message: IMessage = <IMessage>JSON.parse(msg.content.toString())

        if(message.authInfo && message.payload) {
            const order: IOrderDocument = <IOrderDocument> message.payload;
            order._id= null;
            if(await authValid(message.authInfo.token)) {
                const service = new orderService(); 
                await service.create(order);
                console.log(`Create a new order succesfully`);        
            } else {
                console.log(`Cannot receive the properly valid token`);      
            }
        } else {
            // Display the received message
            console.log(`Cannot receive the properly order`);
        }
        // Acknowledge the message
        channel.ack(msg);
    }
}

const getConnection = async (): Promise<Connection> => {
    return await client.connect(config.queueStringConncetion);
}

const getChannel = async(connection: Connection): Promise<Channel> => {
    const channel: Channel = await connection.createChannel();
    await channel.assertQueue(config.queueChannel);
    return channel;
}

const sendMessage = async(message:string) => {
    const channel:Channel = await getChannel(await getConnection());
    channel.sendToQueue(config.queueChannel, Buffer.from(`Sent from Orders: ${message}`));
}  

const receiveMessage = async (): Promise<boolean> => {
    try {
        const channel:Channel = await getChannel(await getConnection());
        await channel.consume(config.queueChannel, consumer(channel));
        return true;
    } catch(error) {
        return false;
    }
}  

export { sendMessage, receiveMessage };