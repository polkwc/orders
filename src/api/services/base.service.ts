import mongoose, {Model} from 'mongoose';
import IBase from '../interfaces/base.interface';
import config from '../config';

export class baseService<T extends IBase, TE extends Model<IBase>> {
    _model: TE;
    // _model: Model<Document>;
    constructor(model: TE) {

    this._model = model;

    const mongoProtocol = config.mongoProtocol;
    const mongoUser = config.mongoUser;
    const mongoPassword = config.mongoPassword;
    const mongoHost = config.mongoHost;
    const mongoPort = config.mongoPort;
    const mongoDataBase = config.mongoDataBase;
    const connectionString = `${mongoProtocol}://${mongoUser}:${mongoPassword}@${mongoHost}:${mongoPort}/${mongoDataBase}`
    // mongoose.connect('mongodb://ms02user:admin123@192.168.16.104:27017/ms02')
    mongoose.connect(connectionString)
        .then(result=> {
            console.log('Database running well')
        })
        .catch(err=> {
            console.log(`Database error ${err}`)
        });
    }

    get model() {
        return this._model;
    }
    set model(model) {
        this._model = model;
    }

    async getAll(): Promise<T[]> {
        // Fetch all Todo’s from the database and return as payload
        return await this._model.find({});
    }

    async getById(id:string): Promise<T> {
        // Find Todo based on id, then toggle done on/off
        return  this._model.findById(id);
        // return p;
    }

    async create(instance: IBase) {
        return await this._model.create(instance)
    }

    async update(id: string, instance: IBase) {
        return await this._model.findByIdAndUpdate(id, instance, { new: true })
    }

    async remove(id: string) {
        return await this._model.findByIdAndRemove(id)
    }
}