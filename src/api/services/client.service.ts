import { clientModel } from '../models/client.model';
import { baseService } from './base.service.js';
import { IClientDocument} from '../interfaces/client.interface';

export class clientService extends baseService<IClientDocument, typeof clientModel> {
  constructor() {
      super(clientModel);
  }

  async getClientByCode(code: string) : Promise<IClientDocument> {
    const data = await clientModel.findOne({code});
    return data;
  }

  // async getUsers() {
  //   return new Promise((resolve) => {
  //     setTimeout(function () {
  //       resolve(fake.userData);
  //     }, 1000);
  //   });
  // }

  // async getClientData(clientId) {
  //   return new Promise((resolve) => {
  //     setTimeout(function () {
  //       resolve(fake.userData.find((x) => x.id === Number(userId)));
  //     }, 1000);
  //   });
  // }

  // async getClientData(userId) {
  //   return new Promise((resolve) => {
  //     setTimeout(function () {
  //       const user = fake.userData.find((x) => x.id === Number(userId));
  //       resolve(fake.clientData.find((x) => x.id === user?.clientId));
  //     }, 1000);
  //   });
  // }
}
