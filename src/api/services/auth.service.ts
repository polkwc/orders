import jwtDecode, { JwtPayload } from 'jwt-decode';
import { IJwtDecoded } from '../interfaces/jwtdecoded.interface';
import { clientService } from '../services/client.service';

const authValid = async (token:string): Promise<boolean> => {
    const jwtDecoded: IJwtDecoded = jwtDecode<JwtPayload>(token); 

    const service = new clientService();
    let expirationTime = Date.now();
    if (jwtDecoded.expires <= expirationTime) {
        // Expiration time is defeated
        return false;
    }
    return await service
        .getById(jwtDecoded.id.toString())
        .then((userInstance) => {
            // No user found with that username
            if (!userInstance) {
                return false;
            }
            let isMatch = false;
            // Make sure the password is correct
            isMatch = userInstance.password === jwtDecoded.password;
            // Password did not match
            if (!isMatch) {
                return false;
            }
            // Success
            return true;
        });
}

export { authValid };