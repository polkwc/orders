// import * as httpMessage from '../helpers/httpMessages.js';
import express from 'express';
import config from '../config.js';
import passport from 'passport';
import jwt from 'jsonwebtoken';

import { clientService } from '../services/client.service';


export const login = (req: express.Request, res: express.Response) => {
  const code = req.body.code;
  const password = req.body.password;
  const jwtOptions: any = {};
  jwtOptions.secretOrKey = config.authJwtSecret;

  const service = new clientService();
  return service
    .getClientByCode(code)
    .then((client) => {
      if (!client) {
        res.status(404).json({ error: { code: '001', message: 'Client is not found'} })
        // throw Error('401 - User is not found');
      }
      if (!(client.password === password)) {
        res.status(404).json({ error: { code: '002', message: 'Client or password is not match'} })
      }
      let expirationTime = getTimeExpirationMaxRange();
      let payload = {
        id: client.id,
        code: client.code,
        password: client.password,
        expires: expirationTime,
      };
      let token = jwt.sign(payload, jwtOptions.secretOrKey);
      return res.status(201).json({ status: 'ok', token: token, client: client });
    })
    .catch((err) => {
      res.status(404).json({ error: { code: '002', message: 'Issues happened'} })
    });
};
import passportJWT from 'passport-jwt';
export const init = () => {
  const ExtractJwt = passportJWT.ExtractJwt;
  const JwtStrategy = passportJWT.Strategy;
  const jwtOptions:any = {};
  jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  jwtOptions.secretOrKey = config.authJwtSecret;
  // passport = passportParam;
  const service = new clientService();
  passport.use(
    new JwtStrategy(jwtOptions, function (jwt_payload, callback) {
      service;
      let expirationTime = Date.now();
      if (jwt_payload.expires <= expirationTime) {
        // Expiration time is defeated
        return callback(null, false);
      }
      service
        .getById(jwt_payload.id.toString())
        .then((userInstance) => {
          // No user found with that username
          if (!userInstance) {
            return callback(null, false);
          }
          let isMatch = false;
          // Make sure the password is correct
          isMatch = userInstance.password === jwt_payload.password;
          // Password did not match
          if (!isMatch) {
            return callback(null, false);
          }
          // Success
          return callback(null, userInstance);
        })
        .catch((error) => {
          console.log(error);
          return callback(error);
        });
    })
  );
};

export const getTimeExpirationMaxRange = () => {
  let timeExpirationMinutes = config.authTimeExpirationMinutes;
  var currentDate = new Date();
  //Add the minutes to current date to arrive at the new date
  currentDate.setMinutes(currentDate.getMinutes() + Number(timeExpirationMinutes));
  return currentDate.getTime();
};

// export const valid = (ctx) => {
//   return { ok: 'everithing is ok' };
// };
