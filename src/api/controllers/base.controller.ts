import express from 'express';
import {Model} from 'mongoose';
import IBase from '../interfaces/base.interface';
import { baseService } from '../services/base.service';

export class baseController<T extends IBase, TE extends Model<IBase>> {
    _entity: T;
    _service: baseService<T,TE> = null;
  
    // constructor(public readonly entity: T, service: baseService) {
    constructor(service: baseService<T, TE>) {
      this._service = service;
      // this._entity = entity;
    }
  
    getAll = async (req: express.Request, res: express.Response) => {
      const dataSet = await this._service.getAll();
      res.status(201).json({ data: dataSet })
    };
  
    getById = async (req: express.Request, res: express.Response) => {
      const { id }= req.params;
      const dataSet = await this._service.getById(id);
      if (!dataSet) throw new Error('BROKEN');
      res.status(201).json({ data: dataSet })
    };
  
    create = async (req: express.Request, res: express.Response): Promise<any> => {
      this._entity = req.body;
      const objectCreated = await this._service.create(this._entity);
      return res.status(201).json({ data: objectCreated });
    }

    update = async (req: express.Request, res: express.Response): Promise<any> => {
      const { id }= req.params;
      this._entity = req.body;
      const updatedCreated = await this._service.update(id, this._entity);
        return res.status(201).json({ data: updatedCreated });
    };
  
    delete = async (req: express.Request, res: express.Response): Promise<any> => {
      const { id }= req.params;
      const objectDeleted = await this._service.remove(id);
      return res.status(201).json({ data: objectDeleted });
    };
  }
  