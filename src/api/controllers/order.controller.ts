
import express from 'express';
import { orderService } from '../services/order.service';
import { sendMessage } from '../services/broker.service';
import { orderModel } from '../models/order.model';
import { baseController } from './base.controller.js';
import { IOrderDocument } from '../interfaces/order.interface';

export class orderController extends baseController<IOrderDocument, typeof orderModel> {

    constructor() {
        super(new orderService());
    }
   
    testMessageBroker = async (req: express.Request, res: express.Response) => {
        const { message }= req.params;

        await sendMessage(message);
    };
}
