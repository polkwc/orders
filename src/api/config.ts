import dotenv from 'dotenv';
dotenv.config(); 

const config = {
  version: process.env.API_VERSION || '0.0.0',
  name: process.env.API_NAME || 'rest-api',
  
  env: process.env.API_ENV || 'development',
  
  host: process.env.API_HOST || 'localhost',
  port: process.env.API_PORT || 5000,
  
  mongoProtocol: process.env.MONGO_PROTOCOL,
  mongoUser: process.env.MONGO_USER,
  mongoPassword: process.env.MONGO_PASSWORD,
  mongoHost: process.env.MONGO_HOST,
  mongoPort: process.env.MONGO_PORT,
  mongoDataBase: process.env.MONGO_DATABASE,

  authJwtSecret: process.env.AUTH_JWT_SECRET || 'empty',
  authTimeExpirationMinutes: process.env.AUTH_TIME_EXPIRATION_MINUTES || 10,

  queueStringConncetion: process.env.QUEUE_STRING_CONNECTION || 'empty',
  queueChannel: process.env.QUEUE_CHANNEL || 'empty',

};

export default config;
