import express, { Router } from 'express';
import * as controller from '../controllers/auth.controller';
const router = Router();
// const controller = new authController();

const init = (auth:any) => {
  // router.get('/valid', auth.authenticate('jwt', { session: false }), async (ctx) => {
  //   ctx.body = await controller.valid(ctx);
  // });

  // router.get('/isAlive', async (ctx) => {
  //   ctx.body = await controller.isAlive(ctx);
  // });

  // router.post('/login', async (req: express.Request, res: express.Response) => {
  //   ctx.body = await controller.login(ctx);
  // });

  router.post('/login', controller.login);
  
  return router;
};
export {init} ;
