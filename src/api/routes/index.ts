import { Router } from 'express';
import * as orderRoute from './order.route';
import isAliveRoute from './isalive.route';
import * as authRoute  from './auth.route';

import * as authentication from '../controllers/auth.controller.js';

const init = () => {
    authentication.init();
};

const getRouters = (passport:any) => {
    let routers = Router();
    routers.use('/order', orderRoute.init(passport));
    routers.use('/isAlive', isAliveRoute);
    routers.use('/auth', authRoute.init(passport));
    
    
    return routers;
};

export {init, getRouters};
