import express, { Router } from 'express';
import passport from 'passport';
import { orderController } from '../controllers/order.controller';

const router = Router();
const controller = new orderController();

const init = (auth: any) => {

    router.get('/', auth.authenticate('jwt', {session: false}), controller.getAll);
    router.get('/:id', auth.authenticate('jwt', {session: false}),controller.getById);
    router.post('/', auth.authenticate('jwt', {session: false}),controller.create); 
    router.put('/:id', auth.authenticate('jwt', {session: false}),controller.update); 
    router.delete('/:id', auth.authenticate('jwt', {session: false}),controller.delete); 

    return router;
}
export {init};
