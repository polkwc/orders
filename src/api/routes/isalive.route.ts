import express, { Router } from 'express';
const router = Router();
import {orderController} from '../controllers/order.controller';

router.get('/', async(request: express.Request, response: express.Response) => {
    const data = {
        type: 'single',
        status: 'ok',
    };
    response.json(data);
})

router.get('/broker/:message', new orderController().testMessageBroker);

export default router;
