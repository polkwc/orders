import { model, Schema, Model, Document } from 'mongoose';
import { IOrderModel, IOrderDocument, IOrderItemDocument} from '../interfaces/order.interface';

const orderItemSchema: Schema<IOrderItemDocument> = new Schema({
    id: { type: Number, required: true },
    title: { type: String, required: true },
    vendor: { type: String, required: true },
    productType: { type: String, required: true },
    amount: { type: Number, required: true },
    price: { type: Number, required: true },
});

const orderSchema: Schema<IOrderDocument> = new Schema({
    customerId: { type: String, required: true },
    amount: { type: Number, required: true },
    price: { type: Number, required: true },
    createdAt: { type: Date, required: true },
    updatedAt: { type: Date, required: true },
    items: [orderItemSchema]
});

const orderModel:Model<IOrderDocument> = model<IOrderDocument, IOrderModel>('order', orderSchema);
export { orderSchema, orderModel };
