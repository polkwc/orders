import { model, Schema, Model, Document } from 'mongoose';
import { IClientModel, IClientDocument } from '../interfaces/client.interface';

const clientSchema: Schema<IClientDocument> = new Schema({
    code: { type: String, required: true },
    password: { type: String, required: true },
});

const clientModel:Model<IClientDocument> = model<IClientDocument, IClientModel>('client', clientSchema);
export { clientSchema, clientModel };
