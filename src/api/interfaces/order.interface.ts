import { Document, Model } from "mongoose";
import IBase from './base.interface';

interface IOrderItem extends IBase {
    title: string
    vendor: string
    productType: string
    amount: number
    price: number
}

interface IOrder extends IBase {
    customerId: string,
    amount: number
    price: number
    createdAt: Date
    updatedAt: Date
    items: [IOrderItem]
}

interface IOrderDocument extends IOrder, Document {
    id: number;
}

interface IOrderItemDocument extends IOrderItem, Document {
    id: number
}
interface IOrderModel extends Model<IOrderDocument> { 
}

export { IOrder, IOrderModel, IOrderItem, IOrderDocument, IOrderItemDocument };
