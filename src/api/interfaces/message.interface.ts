import { IOrder } from '../interfaces/order.interface';

interface IMessage {
  payload: IOrder,
  authInfo: {
    token: string,
  };
}

export { IMessage };