
interface IJwtDecoded {
    code?: string, 
    password?: string, 
    expires?: number, 
    iat?: number,
    id?: string
}

export { IJwtDecoded };