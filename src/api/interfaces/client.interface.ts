import { Model, Document } from 'mongoose';
import IBase from './base.interface';

interface IClient extends IBase {
  code: string;
  password: string;
}

interface IClientDocument extends IClient, Document {
  id: number;
}

interface IClientModel extends Model<IClientDocument> { 
}

export { IClient, IClientModel, IClientDocument };