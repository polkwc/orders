import express from 'express';

import bodyParser from 'body-parser';
import errorMiddleware from './api/middlewares/error.middleware';
import config  from './api/config';
import passport from 'passport';
import session from 'express-session';
import { receiveMessage } from './api/services/broker.service';

const app = express();
app.use(bodyParser.json());

import  { getRouters, init } from './api/routes';

app.use(session({
  secret: 'r8q,+&1LM3)CD*zAGpx1xm{NeQhc;#',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60 * 60 * 1000 } // 1 hour
}));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
app.use(passport.session());

init();

app.use(getRouters(passport));
app.use(errorMiddleware);

const host = config.host;
const port = config.port;
const name = config.name;
const version = config.version;

// start the server
app.listen(port, async() => {
  console.log(`${name} ${version} -- server running on ${host}:${port}`);

  const connected = await receiveMessage();
  console.log(connected ? `Channel ${config.queueChannel} working well`: `Issues in the channel`);
});
