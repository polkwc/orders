FROM node:alpine

WORKDIR /app
COPY package.jsno ./
RUN yarn install
COPY ./ ./
CMD ["npm", "start"] 