# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

* Quick summary: Challenge for Thirdlove
* Owner: Marcelo Garcia Schwarz
* Version 1.0.0


## How do I get set up? ##

* git clone https://polkwc@bitbucket.org/polkwc/orders.git
* yarn install (before you have to install yarn)
* yarn start (to run code)

## Configuration file ##
you have to set up your own variable in the file named .env

Custom environment 
- API_ENV = dev
- API_HOST = localhost
- API_PORT = 8081

You have to update the values for wheater service
- WHEATER_API_ENDPOINT
- WHEATER_API_API_ID
- WHEATER_API_UNITS
- WHEATER_API_EXCLUDE
- WHEATER_API_TTL

Values for the mongo data base (here we save data related to the orders throught en Queue)
- MONGO_PROTOCOL
- MONGO_USER
- MONGO_PASSWORD
- MONGO_HOST
- MONGO_PORT
- MONGO_DATABASE

For this database we need a valid client (one or more than one) with the following structure
```sh
{  "_id": {   
        "$oid": "625e823823d82ce52dee1a14" 
    },  
    "code": "001",  
    "password": "password123"
}
```

Values used to create the JWT when some client require a authentication token
- AUTH_JWT_SECRET = 'XXXXXXXXXXXXXXXXXXXXXXXX'
- AUTH_TIME_EXPIRATION_MINUTES = 2400

And last, this process uses a RabbitMQ query to manage message between Orders API and Carts API
- QUEUE_STRING_CONNECTION = amqps://some_url_api.com/evggroxy
- QUEUE_CHANNEL = MANAGE_ORDERS

## How to use ##

* There is an endpoint to get all orders created

### For the city resourses

* Request to get a valid token (code and password are client data when we perform a B2B connection)
```sh
curl --request POST \
  --url http://localhost:8081/auth/login \
  --header 'Content-Type: application/json' \
  --data '{
  "code": "001",
  "password": "password123"
}'
```

Response example 
```sh
{
	"status": "ok",
	"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyNWU4MjM4MjNkODJjZTUyZGVlMWExNCIsImNvZGUiOiIwMDEiLCJwYXNzd29yZCI6InBhc3N3b3JkMTIzIiwiZXhwaXJlcyI6MTY1MDc4MzUzNTc1OSwiaWF0IjoxNjUwNjM5NTM1fQ.gx8N1VLKOS4Tv9kL9gwZ1m3j-0XK8E9nWC6EXUOoXJA",
	"client": {
		"_id": "625e823823d82ce52dee1a14",
		"code": "001",
		"password": "password123"
	}
}
```
This API validate in a queue message if the token provided is valid

* Get all orders
```sh
curl --request GET \
  --url http://localhost:8081/order \
  --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyNWU4MjM4MjNkODJjZTUyZGVlMWExNCIsImNvZGUiOiIwMDEiLCJwYXNzd29yZCI6InBhc3N3b3JkMTIzIiwiZXhwaXJlcyI6MTY1MDc1MzQ0OTgwOSwiaWF0IjoxNjUwNjA5NDQ5fQ.zprPbyfwbLf7iJmNdfqwGrycr25fj9Tk4I3w4F5cRD0'

```

* Also you can create an order (but this has to be created througth the queue message from Carts API to Orders API )
```sh
curl --request POST \
  --url http://localhost:8081/order \
  --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyNWU4MjM4MjNkODJjZTUyZGVlMWExNCIsImNvZGUiOiIwMDEiLCJwYXNzd29yZCI6InBhc3N3b3JkMTIzIiwiZXhwaXJlcyI6MTY1MDc1MzQ0OTgwOSwiaWF0IjoxNjUwNjA5NDQ5fQ.zprPbyfwbLf7iJmNdfqwGrycr25fj9Tk4I3w4F5cRD0' \
  --header 'Content-Type: application/json' \
  --data '{
    "customerId": "12121313",
    "amount": 6,
    "price": 150,
    "createdAt": "2022-04-20T13:30:46-08:00",
    "updatedAt": "2022-04-20T13:30:46-08:00",
    "items":
    [
        {
            "id": 512475791422,
            "title": "24/7™ Classic Contour Plunge Bra",
            "vendor": "ThirdLove",
            "productType": "Bra",
            "amount": 4,
            "price": 50
        },
        {
            "id": 512485785662,
            "title": "24/7™ Classic Contour Plunge Bra",
            "vendor": "ThirdLove",
            "productType": "Bra",
            "amount": 2,
            "price": 100
        }
    ]
}'

```
